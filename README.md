# Group 5 - Case Vipps

A distributed software solution for accepting orders for a digital product, using a 3rd party payment processor.

### Group members

- @anetteOlli - Anette Olli Siiri
- @Patrickpt - Patrick Thorkildsen
- @Eiriksaur - Eirik Eide Saur
- @SteinarP - Steinar Valheim Pedersen
- @NbdNguyen - Nghi Nguyen

## Project Overview:

- A web front-end (written [here](https://gitlab.com/anetteOlli/experis-vipps-client))
  - A landing page: login (anynomous user authenticates) or purchase (anynomous user proceeds to payment gateway)
  - A purchase page: 0-4 generated random items
- An [API](https://documenter.getpostman.com/view/13215254/TVYDffDS) that receives data from the front-end and communicates with the database
  - GET /order, /order\{order_id}
  - POST /order
- A Database
  - Customer: A table with all the customers
  - Item: A table with all the items
  - Receipt: A table with all the orders/receipts
  - Receipt_items: A table with receiptId's and itemId's to know which receipts contains which items.
- A 3rd party payment processor
  - Stripe: to pay with

## Tech - Dependencies

- Spring Initializr
  - Spring Security
  - Spring web
  - OAuth2Client
  - Spring Boot DevTools
  - PostgreSQL Driver
- Stripe
- PostgreSQL Database

## Installation instructions

### Setting up the server:

The server requires a postgres database.

#### The database requires the following user environment variables:

| User Env Variable | Value                                                                      |
| ----------------- | -------------------------------------------------------------------------- |
| VIPPS_DB_URL      | `jdbc:postgresql://url_your_database:port_your_database/your_databasename` |
| VIPPS_DB_USERNAME | _your_username_                                                            |
| VIPSS_DB_PASSWORD | _your_password_                                                            |

### Using the e-mail service

The backend utilizes a smtp (Simple Mail Transfer Protocol) e-mail service.
It requires a Gmail account and the following environment variables:

| User Env Variable | Value                                                                      |
|-------------------|----------------------------------------------------------------------------|
| SMTP_USERNAME     | *Your gmail account username*                                              |
| SMTP_PASSWORD     | *Your gmail account password*                                              |

### Using Stripe

The system utilizes a third-party-application to handle payments, called Stripe.
Using Stripe requires an account which can be created at [https://stripe.com/en-no](https://stripe.com/en-no).
After creating the account retrieve your secret and public key from the website and use them as follows:

| User Env Variable | Value                                                                      |
|-------------------|----------------------------------------------------------------------------|
| STRIPE_SECRET_KEY | *Your secret key from Stripe*                                               |

The public key needs to be added in the file `/case-vipps/client/src/constats/constants.js`

| Constants.js var  | Value                                                                      |
|-------------------|----------------------------------------------------------------------------|
| Stripe_PK         | *Your public key from Stripe*                                              |


#### Creating and using Google credentials:

Create a OAuth client ID at https://console.developers.google.com/apis/credentials
The oauth client id will require the following:

![](/images/google_credentials.PNG)

Finally you need to set user environment variables for the google id:

| User env variable         |                             |
| ------------------------- | --------------------------- |
| VIPPS_OAUTH_GOOGLE_ID     | _your_google_client_id_     |
| VIPPS_OAUTH_GOOGLE_SECRET | _your_google_client_secret_ |

Remember to reboot computer after editing user environment variables on windows systems.

```console
cd server
mvn package
java -jar /target/vipps-0.0.1-SNAPSHOT.jar
```


## See also:
- ### [Database Documentation](https://gitlab.com/nbdnguyen/case-vipps/-/blob/master/documentation/DATABASE-DOCUMENTATION.md)
- ### [API Documentation](https://documenter.getpostman.com/view/13215254/TVYDffDS)
- ### [User Manual](https://gitlab.com/nbdnguyen/case-vipps/-/blob/master/documentation/USER-MANUAL.md)
