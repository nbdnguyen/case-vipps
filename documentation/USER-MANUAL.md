**4.3.1**: Candidates should submit a user manual containing instructions for how to perform each action within the system with accompanying screenshots.
___ 

# Introduction
These instructions assume the service has already been setup. If not, see installation instructions at [README](https://gitlab.com/nbdnguyen/case-vipps/-/blob/master/README.md) file.

When you enter the webpage this will be the landing welcome page. The header will be available throughout the site, and clicking the *ONLINE STORE* text logo on the left side will redirect back to the homepage.


<img src="/uploads/aac25d079a5db6722b885afc2e42188f/image.png" width="400">


## Register
You can register a new account by clicking the *REGISTER*
button on the landing page. Here you can either register
and log in with an existing Google account,
or register a new account without Google.
<!-- Insert image of login page -->

<img src="/uploads/3b3dfa282755937739be3822a7eab7dc/image.png" width="400">


If you register without Google you need to verify your
email before being able to log in. You will be 
redirected to a new page after registering. To
verify your account, enter the 5 digit code 
sent to the email address you registered with 
on this page.
<!-- Insert image of verification page  -->

<img src="/uploads/c527956e9c311dd906b8e88a850d053e/image.png" width="400">


## Login
From the landing page or the header you can click on *LOG IN*
with Google or an account you have created with us.
<!-- Insert image of login page -->

<img src="/uploads/4c4fcb4f319c45caa2b4d488178ec295/image.png" width="400">


Should you choose to not log in with Google you
need to verify your login by entering a code sent to
your email. After logging in you will be redirected
back to the landing page.

<img src="/uploads/42868db38952cc3f5b5f2eb7516897aa/image.png" width="400">

## Forgotten password

If you have forgotten your password you can click the *FORGOT PASSWORD* under the login box. It will redirect you to a page where you can write your email to the account you forgot the password of.

<img src="/uploads/6debf3735f2ed9431471333333343911/image.png" width="400">


If your email address was registered a mail will be sent with a verification code. A new schema will appear underneath the current one where you can input the verification code as well as a new password, which will be saved. You can then proceed to log in with the new password.

<img src="/uploads/8ec457dadeef4ff013a34857386801bd/image.png" width="400">


After you have logged in the landing page buttons will have changed to better fit a logged in customer.


<img src="/uploads/431291b8e4ef7f1b348a8461db387f44/image.png" width="400">


## Place order
To place an order click the *PURCHASE* button on the
landing page. A cart of 0-4 items will be generated for you, 
as specified in the Requirement Specifications.
To place your order you need to fill in your email, name and address.
If you are logged in, and your account has this information
attached to it the fields will be autofilled, but can
still be changed if the information is incorrect. 
If you are not logged in then the fields will be blank, 
and you have to fill them out before you pay.
<!-- Insert image of purchase page  -->

<img src="/uploads/a201d3cc0c879a04aedd4e8a40161f3f/image.png" width="600">


If zero items were generated there will be an alert to inform you. Just refresh to page to try to generate some items again.

<img src="/uploads/b2eb84bbccb1569fa72114e264700d8d/image.png" width="400">


When you have filled out the required information,
click save. A new button *Pay With Card* will be shown, letting you
go to checkout through Stripe. To test payment the
following test card can be used:

| Card number        | CVC           | Date            |
| ------------------ | ------------- | --------------- |
| 4242 4242 4242 4242| Any 3 numbers | Any future date |

<!-- Insert image of Stripe popup  -->
<img src="/uploads/d36483f1b1c8b27f3205516004ed7d9a/image.png" width="400">

If the payment is succsessful you will be redirected to your receipt page, and the most recent receipt will be shown.
Should you receive an error message you can safely try purchasing
again; you will not be charged twice.

## Browse previous orders
If you are logged in you will be able to view all receipts of orders you
have previously completed or attempted. To do this click the *RECEIPTS*
button from the landing page. Each receipt can be clicked to show more
detailed information.
<!-- Insert image of receipts page  -->

<img src="/uploads/253df02f9b7125f1787ce6cc9d61fa6f/image.png" width="400">

## Edit account information
To edit your account information you need to be logged in. You can
then click the avatar on the right side of the header to go to your profile, or click the *PROFILE* button on the homepage.
Here you can edit your first name, last name, address and phone number.
When you are done, click *UPDATE*. The page will then refresh to show the updated information. Updating your email is not possible.
<!-- Insert image of profile page  -->

<img src="/uploads/1e492c7c4a773067257368f161f9a4ae/image.png" width="400">

## Resetting password
If you have forgotten the password you registered with (not for Google accounts)
you can reset your password from the login page. Click *Forgot password*
and enter the email you registered with on the page you are 
redirected to. You will receive an email with a code that you
can use to update your password in the form that opens.
<!-- Insert image of reset password page  -->

<img src="/uploads/b53cbe219fb4d81507764f6cff8afdcd/image.png" width="400">