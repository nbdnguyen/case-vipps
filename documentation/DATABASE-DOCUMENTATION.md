# Database Documentation

## Includes:

<img src="/uploads/c396a504e7a42ddaaaebd3fb94ccf703/image.png">

### Table Dependents - This could be wrong, check it.

Customer:
- customer_id: PRIMARY KEY

Item:
- item_id: PRIMARY KEY

Receipt:
- receipt_id: PRIMARY KEY

Receipt_items - a table containing keys for which items are in which receipts:
- receipt_items_id: PRIMARY KEY
- receipt_id: FOREIGN KEY
- item_id: FOREIGN KEY

### Table Relationships
- Customer - one to many - receipt, bidirectional
- Receipt - many to one - customer, bidirectional
- Receipt - many to many - item, unidirectional
