package no.noroff.vipps.controllers;

import no.noroff.vipps.security.UserPrincipal;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class ReceiptControllerTest {
    ReceiptController controller = new ReceiptController();

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllReceipts() {
        UserPrincipal userPrincipal = new UserPrincipal((long) 6, "1@abc.com", null, null);
        ResponseEntity re = controller.getAllReceipts(userPrincipal);
        Assert.assertEquals(re.getStatusCode(), HttpStatus.OK);
    }

    @Test
    void getReceipt() {
    }

    @Test
    void addNewReceipt() {
    }

    @Test
    void updateReceipt() {
    }

    @Test
    void deleteReceipt() {
    }
}