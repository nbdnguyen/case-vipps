package no.noroff.vipps.controllers;

import no.noroff.vipps.models.Item;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class ItemControllerTest {

    ItemController controller = new ItemController();

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllItems() {
        ResponseEntity re = controller.getAllItems();
        Assert.assertEquals(re.getStatusCode(), HttpStatus.OK);
    }

    @Test
    void getItem() {
    }

    @Test
    void addNewItem() {
        Long id = 100L;
        controller.deleteItem(id);

        Item item = new Item(id, "Kaffekopp", 30);
        ResponseEntity reAdd = controller.addNewItem(item);

        Assert.assertEquals(reAdd.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    void updateItem() {
    }

    @Test
    void deleteItem() {
    }
}