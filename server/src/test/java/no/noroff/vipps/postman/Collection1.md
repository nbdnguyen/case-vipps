-   [General notes](#doc-general-notes)
-   [API detail](#doc-api-detail)
-   [all orders](#request-all-orders)
-   [order 1](#request-order-1)
-   [new order](#request-new-order)

Vipps NoAuth
============

General notes [**](#doc-general-notes) {#doc-general-notes}
--------------------------------------

API detail [**](#doc-api-detail) {#doc-api-detail}
--------------------------------

### all orders [**](#request-all-orders) {#request-all-orders}

-   [Curl](#request-all-orders-example-curl)
-   [HTTP](#request-all-orders-example-http)

<!-- -->

    curl -X GET "http://www.localhost:8080/api/v1/order"

    GET /api/v1/order HTTP/1.1
    Host: www.localhost:8080

* * * * *

### order 1 [**](#request-order-1) {#request-order-1}

-   [Curl](#request-order-1-example-curl)
-   [HTTP](#request-order-1-example-http)

<!-- -->

    curl -X GET "http://www.localhost:8080/api/v1/order/1"

    GET /api/v1/order/1 HTTP/1.1
    Host: www.localhost:8080

* * * * *

### new order [**](#request-new-order) {#request-new-order}

-   [Curl](#request-new-order-example-curl)
-   [HTTP](#request-new-order-example-http)

<!-- -->

    curl -X POST -d '{
        "userId": "1",
        "itemId": "5"
    }' "http://www.localhost:8080/api/v1/order"

    POST /api/v1/order HTTP/1.1
    Host: www.localhost:8080

    {
        "userId": "1",
        "itemId": "5"
    }

* * * * *
