INSERT INTO customer(email, given_name) VALUES ('harriet.cullen@gmail.com', 'Harriet Cullen');
INSERT INTO customer(email, given_name) VALUES ('nusaybah.nairn@live.com', 'Nusaybah Nairn');
INSERT INTO customer(email, given_name) VALUES ('cairo.stephens@hotmail.com', 'Cairo Stephens');
INSERT INTO customer(email, given_name) VALUES ('seamus.gaines@gmail.com', 'Seamus Gaines');
INSERT INTO customer(email, given_name) VALUES ('loki.nolan@gmail.com', 'Loki Nolan');

INSERT INTO item(name, price) VALUES ('Apple', 3);
INSERT INTO item(name, price) VALUES ('Gucci hat', 10000);
INSERT INTO item(name, price) VALUES ('Boat', 30000);
INSERT INTO item(name, price) VALUES ('Pasta', 25);
INSERT INTO item(name, price) VALUES ('Milk', 27);
INSERT INTO item(name, price) VALUES ('Cool t shirt', 150);
INSERT INTO item(name, price) VALUES ('Blue jeans', 600);

INSERT INTO receipt(status, customer_id) VALUES ('COMPLETE', 2);
INSERT INTO receipt(status, customer_id) VALUES ('COMPLETE', 5);
INSERT INTO receipt(status, customer_id) VALUES ('COMPLETE', 2);
INSERT INTO receipt(status, customer_id) VALUES ('IN PROGRESS', 2);
INSERT INTO receipt(status, customer_id) VALUES ('COMPLETE', 3);
INSERT INTO receipt(status, customer_id) VALUES ('IN PROGRESS', 4);

INSERT INTO receipt_items(receipt_receipt_id, items_item_id) VALUES (1, 3);
INSERT INTO receipt_items(receipt_receipt_id, items_item_id) VALUES (2, 2);
INSERT INTO receipt_items(receipt_receipt_id, items_item_id) VALUES (3, 4);
INSERT INTO receipt_items(receipt_receipt_id, items_item_id) VALUES (4, 3);
INSERT INTO receipt_items(receipt_receipt_id, items_item_id) VALUES (5, 5);
INSERT INTO receipt_items(receipt_receipt_id, items_item_id) VALUES (2, 2);
INSERT INTO receipt_items(receipt_receipt_id, items_item_id) VALUES (4, 2);
INSERT INTO receipt_items(receipt_receipt_id, items_item_id) VALUES (1, 1);
INSERT INTO receipt_items(receipt_receipt_id, items_item_id) VALUES (2, 4);



