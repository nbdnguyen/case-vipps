package no.noroff.vipps.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.io.InputStream;

@Component
public class EmailServiceComponent{

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendVerificationEmail(String recipient, String verificationCode) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(recipient);
        msg.setSubject("Registration to Vipps");
        msg.setText("Your verification code is: " +
                verificationCode + "\nPlease enter it to verify your email.");
        javaMailSender.send(msg);
    }

    public void sendLoginVerification(String recipient, String verificationCode) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(recipient);
        msg.setSubject("Login verification to Vipps");
        msg.setText("Your verification code is: " +
                verificationCode + "\nPlease enter it to login.");
        javaMailSender.send(msg);
    }

    public void sendResetEmail(String recipient, String verificationCode) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(recipient);
        msg.setSubject("Reset password Vipps");
        msg.setText("Your verification code is: " +
                verificationCode + "\nPlease enter it to reset your Password.");
        javaMailSender.send(msg);

    }

}
