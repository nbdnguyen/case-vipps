package no.noroff.vipps.payload;

/*
Simple class to help send data from front to backend when resetting password
 */
public class CustomerPasswordResetObject {
    private String verificationKey;
    private String username;
    private String password;

    public String getVerificationKey() {
        return verificationKey;
    }

    public void setVerificationKey(String verificationKey) {
        this.verificationKey = verificationKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
