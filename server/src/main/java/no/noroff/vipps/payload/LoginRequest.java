package no.noroff.vipps.payload;

/**
 * Code copied from: https://github.com/callicoder/spring-boot-react-oauth2-social-login-demo
 */
public class LoginRequest {

    private String email;

    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}