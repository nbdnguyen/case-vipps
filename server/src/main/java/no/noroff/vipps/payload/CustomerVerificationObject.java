package no.noroff.vipps.payload;

/*
Simple class to help send data from front to backend when verifying customer logins
 */
public class CustomerVerificationObject {
    private String email;
    private String password;
    private String verificationKey;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVerificationKey() {
        return verificationKey;
    }

    public void setVerificationKey(String verificationKey) {
        this.verificationKey = verificationKey;
    }
}
