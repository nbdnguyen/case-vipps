package no.noroff.vipps.payload;

/*
Simple class to help send data from front to backend when utilizing Stripe
 */
public class PurchaseItem {
    private String name;
    private int price;
    private int quantity;
    private String idemKey;

    public PurchaseItem(){};

    public PurchaseItem(String name, int price, int quantity, String idemKey){
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.idemKey = idemKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getIdemKey() {
        return idemKey;
    }

    public void setIdemKey(String idemKey) {
        this.idemKey = idemKey;
    }

    @Override
    public String toString() {
        return "PurchaseItem{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", idemKey='" + idemKey + '\'' +
                '}';
    }
}
