package no.noroff.vipps.payload;

import java.util.List;

public class ReceiptPayload {
    private String status;

    private List<Long> itemsIDs;

    private String idemKey;

    private String name;

    private String email;

    private String address;

    //getters and setters

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Long> getItemsIDs() {
        return itemsIDs;
    }

    public void setItemsIDs(List<Long> itemsIDs) {
        this.itemsIDs = itemsIDs;
    }

    public String getIdemKey() {
        return idemKey;
    }

    public void setIdemKey(String idemKey) {
        this.idemKey = idemKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
