package no.noroff.vipps.security.oauth2.user;

import java.util.Map;

/**
 * Code copied from: https://github.com/callicoder/spring-boot-react-oauth2-social-login-demo
 * And modified
 */
public class GoogleOAuth2UserInfo {
    protected Map<String, Object> attributes;

    public GoogleOAuth2UserInfo(Map<String, Object> attributes) {
        this.attributes = attributes;
    }
    public String getId() {
        return (String) attributes.get("sub");
    }
    public String getName() {
        return (String) attributes.get("name");
    }
    public String getEmail() {
        return (String) attributes.get("email");
    }
    public String getFamilyName(){
        return (String) attributes.get("family_name");
    }
    public String getGivenName(){
        return (String) attributes.get("given_name");
    }

    public void printAttributes(){
        attributes.forEach((k, v) -> System.out.printf("%s %s%n", k, v));
    }
}
