package no.noroff.vipps.repositories;

import no.noroff.vipps.models.Customer;
import no.noroff.vipps.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByEmail(String email);

    boolean existsByEmail(String email);

    Customer findByCustomerId(long customer_id);

    Customer findCustomerByEmail(String email);
}
