package no.noroff.vipps.repositories;

import no.noroff.vipps.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    @Query(value="SELECT * FROM item ORDER BY RANDOM() LIMIT :randomInt", nativeQuery = true)
    List<Item> findRandomItems(@Param("randomInt") Integer randomInt);
}
