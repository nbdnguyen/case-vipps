package no.noroff.vipps.repositories;

import no.noroff.vipps.models.Receipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReceiptRepository extends JpaRepository<Receipt, Long> {

    List<Receipt> findAllByCustomer_CustomerId(long customer_id);

    Receipt findByCustomer_CustomerIdAndReceiptId(long customer_id, long receipt_id);

    Receipt findByIdemKey(String idemKey);
}
