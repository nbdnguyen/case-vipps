package no.noroff.vipps.models;

/**
 * Code copied from: https://github.com/callicoder/spring-boot-react-oauth2-social-login-demo
 */
public enum AuthProvider {
    google, local, deleted
}
