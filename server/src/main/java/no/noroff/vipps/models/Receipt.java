package no.noroff.vipps.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.persistence.Entity;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Set;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "receiptId")
public class Receipt implements Comparable<Receipt>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "receipt_id", nullable = false)
    private Long receiptId;

    @ManyToOne
    @Cascade(value={org.hibernate.annotations.CascadeType.MERGE})
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToMany(cascade = {CascadeType.MERGE})
    //Unidirectional ManyToMany between receipts and items
    @JoinTable(
            name = "receipt_items",
            joinColumns = {@JoinColumn(name = "receipt_id")},
            inverseJoinColumns = {@JoinColumn(name = "item_id")}
    )
    private List<Item> items = new ArrayList<Item>();

    private String status;

    private String idemKey;

    private double totalPrice;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp time;

    public Receipt(){
        // Empty constructor used by Hibernate
    };

    public Receipt(Long receiptId, Customer customer, List<Item> items, String status, Timestamp time, String idemKey, double totalPrice) {
        this.receiptId = receiptId;
        this.customer = customer;
        this.items = items;
        this.status = status;
        this.time = time;
        this.idemKey = idemKey;
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Receipt{" +
                "receiptId=" + receiptId +
                ", customer=" + customer +
                ", items=" + items +
                ", status='" + status + '\'' +
                ", time=" + time +
                '}';
    }


    @JsonGetter("items")
    public Set<String> items(){
        if (items == null) return null;
        return items.stream()
                .sorted(Item::compareTo) //(r1, r2) -> r1.compareTo(r2)
                .map(item -> "/api/v1/item/"+item.getItemId())
                .collect(Collectors.toSet());
    }

    @JsonGetter("customer")
    public String customer(){
        if (customer == null) return null;
        return "/api/v1/customer/"+customer.getCustomerId();
    }

    @Override
    public int compareTo(Receipt other) {
        // Default: Comparing receipt timestamp.
        if (this.time == null || other.time == null){
            return Long.compare(this.getReceiptId(), other.getReceiptId());
        } else {
            return this.getTime().after(other.getTime()) ? 1 : 0; //Test if need to be -1
        }
    }

    public Long getReceiptId() { return receiptId; }

    public void setReceiptId(Long ReceiptId) { this.receiptId = receiptId; }

    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status){this.status = status;}

    public Timestamp getTime() { return time; }

    public void setTime(Timestamp time) { this.time = time; }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getIdemKey() {
        return idemKey;
    }

    public void setIdemKey(String idemKey) {
        this.idemKey = idemKey;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
