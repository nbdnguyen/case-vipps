package no.noroff.vipps.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "customerId")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private Long customerId;

    // Customer data probably needs to be elaborated on later on.
    // Let's see what data the 3rd party auth is able to provide.
    private String email;

    @Column(name = "given_name")
    private String givenName;

    @Column(name = "family_name")
    private String familyName;

    private String address;
    private String phone;

    private Boolean verified;

    @OneToMany(mappedBy = "customer", cascade = {CascadeType.ALL})
    List<Receipt> receipts = new ArrayList<Receipt>();

    public Customer(){
        // Empty constructor used by Hibernate
    }

    public Customer(long customerId, String email, String givenName, String familyName, String address, String phone, Boolean verified) {
        this.customerId = customerId;
        this.email = email;
        this.givenName = givenName;
        this.familyName = familyName;
        this.address = address;
        this.phone = phone;
        this.verified = verified;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", email='" + email + '\'' +
                ", givenName='" + givenName + '\'' +
                ", familyName='" + familyName + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", receipts=" + receipts +
                '}';
    }

    @JsonGetter("receipts")
    public List<String> receipts(){
        if (receipts == null) return null;
        return receipts.stream()
                .sorted(Receipt::compareTo) //(r1, r2) -> r1.compareTo(r2)
                .map(receipt -> "/api/v1/receipt/"+receipt.getReceiptId())
                .collect(Collectors.toList());
    }

    public Long getCustomerId() {
        return customerId;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    private AuthProvider provider;

    private String providerId;


    //we should never ever send password back to anyone (even if it's hashed), therefore JsonIgnore
    @JsonIgnore
    private String password;

    //2fa
    @JsonIgnore
    @Column(name = "verification_key")
    private String verificationKey;

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Receipt> getReceipts() {
        return receipts;
    }

    public void setReceipts(List<Receipt> receipts) {
        this.receipts = receipts;
    }

    public boolean addReceipt(Receipt receipt){
        return receipts.add(receipt);
    }

    public int numberOfReceipts(){
        return receipts.size();
    }

    public AuthProvider getProvider() {
        return provider;
    }

    public void setProvider(AuthProvider provider) {
        this.provider = provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVerificationKey() {
        return verificationKey;
    }

    public void setVerificationKey(String verificationKey) {
        this.verificationKey = verificationKey;
    }

    public Boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
