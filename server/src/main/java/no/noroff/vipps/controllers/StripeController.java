package no.noroff.vipps.controllers;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

import com.stripe.Stripe;
import com.stripe.model.Charge;
import com.stripe.net.RequestOptions;

import net.minidev.json.JSONObject;
import no.noroff.vipps.config.AppProperties;
import no.noroff.vipps.payload.PurchaseItem;
import org.springframework.web.bind.annotation.*;

@RestController
public class StripeController {

    private static Gson gson = new Gson();

    @CrossOrigin
    @RequestMapping(value = "/create-session", method = RequestMethod.POST)
    public String getCheckout(@RequestBody Map bodyData){
        AppProperties appProp = new AppProperties();
        String status = "";
        try {
            //Getting both the item and the token from the input
            JSONObject sourceObject = new JSONObject(bodyData);
            String tokenKey = "theToken";
            JSONObject tokenObject = new JSONObject();
            tokenObject.put(tokenKey, sourceObject.remove(tokenKey));
            PurchaseItem theItem = gson.fromJson(sourceObject.remove("cart").toString().trim(), PurchaseItem.class);
            //Adding the secret key
            Stripe.apiKey = System.getenv("STRIPE_SECRET_KEY");
            //Creating the payment object
            Map<String, Object> params = new HashMap<>();
            params.put("amount", theItem.getPrice());
            params.put("currency", "nok");
            params.put("source", "tok_mastercard");
            //Creating a unique id to ensure no double payments
            RequestOptions options = RequestOptions
                    .builder()
                    .setIdempotencyKey(theItem.getIdemKey())
                    .build();
            Charge charge = Charge.create(params, options);
            status = "Success";
        }
        catch (Exception e){
            e.printStackTrace();
            status = "Failed";
        }
        finally{
            return status;
        }
    }
}

