package no.noroff.vipps.controllers;

import no.noroff.vipps.email.EmailServiceComponent;
import no.noroff.vipps.exception.BadRequestException;
import no.noroff.vipps.models.AuthProvider;
import no.noroff.vipps.models.Customer;
import no.noroff.vipps.payload.CustomerPasswordResetObject;
import no.noroff.vipps.payload.CustomerVerificationObject;
import no.noroff.vipps.payload.ApiResponse;
import no.noroff.vipps.payload.AuthResponse;
import no.noroff.vipps.payload.LoginRequest;
import no.noroff.vipps.payload.SignupRequest;
import no.noroff.vipps.repositories.CustomerRepository;
import no.noroff.vipps.security.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Random;

/**
 * Code copied from: https://github.com/callicoder/spring-boot-react-oauth2-social-login-demo
 *
 * Contains endpoints for logging in and signing up by sending credentials over post.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private EmailServiceComponent emailServiceComponent;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenProvider tokenProvider;

    /*
    //Get the customer from the database based on email
    //Checks that the customer is verified
    //Checks that the password - email combination matches
    //Sets a new verificationkey and sends it by email
    //Return OK if all goes well
     */
    @PostMapping("/login")
    public ResponseEntity<AuthResponse> authenticateUser(@RequestBody LoginRequest loginRequest) {

        HttpStatus status;
        try {
            Customer theCustomer = customerRepository.findCustomerByEmail(loginRequest.getEmail());
            if (theCustomer.isVerified()) {

                Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                loginRequest.getEmail(),
                                loginRequest.getPassword()
                        )
                );

                status = HttpStatus.OK;
                Random random = new Random(System.currentTimeMillis());
                String verKey = Integer.toString(10000 + random.nextInt(20000));

                emailServiceComponent.sendLoginVerification(loginRequest.getEmail(), verKey);

                theCustomer.setVerificationKey(verKey);
                customerRepository.save(theCustomer);

            }else{
                status = HttpStatus.BAD_REQUEST;
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(status);
    }

    /*
    //Check that the email adress is not already in use
    //Make a new verificationkey and send it by email
    //Make the customer object based on the inputdata
    //hash the password and store the customer in the database
    //Return CREATED if all goes well
     */
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody SignupRequest signUpRequest) {
        try {
            if (customerRepository.existsByEmail(signUpRequest.getEmail())) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }else {

                Random random = new Random(System.currentTimeMillis());
                String verKey = Integer.toString(10000 + random.nextInt(20000));

                emailServiceComponent.sendVerificationEmail(signUpRequest.getEmail(), verKey);


                // Creating user's account
                Customer customer = new Customer();
                customer.setGivenName(signUpRequest.getGivenName());
                customer.setFamilyName(signUpRequest.getFamilyName());
                customer.setEmail(signUpRequest.getEmail());
                customer.setAddress(signUpRequest.getAddress());
                customer.setPhone(signUpRequest.getPhone());
                customer.setProvider(AuthProvider.local);
                customer.setVerificationKey(verKey);
                customer.setVerified(false);

                customer.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

                Customer result = customerRepository.save(customer);

                URI location = ServletUriComponentsBuilder
                        .fromCurrentContextPath().path("/api/v1/customer")
                        .buildAndExpand(result.getCustomerId()).toUri();

                return ResponseEntity.created(location)
                        .body(new ApiResponse(true, "User registered successfully"));
            }
        }catch (Exception e){
            e.printStackTrace();
            return  new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    //Make sure that the customer exists
    //Get the customer by
    //Set a new verificationkey
    //Send the key by email to the customer
    //Return OK if everything goes well
     */
    @PutMapping("/resetcode")
    public ResponseEntity resetVerificationCode(@RequestBody CustomerPasswordResetObject customerPasswordResetObject) {
        HttpStatus status;
        try{
            if(customerRepository.existsByEmail(customerPasswordResetObject.getUsername())){
                Customer theCustomer = customerRepository.findCustomerByEmail(customerPasswordResetObject.getUsername());
                Random random = new Random(System.currentTimeMillis());
                String verKey = Integer.toString(10000 + random.nextInt(20000));
                theCustomer.setVerificationKey(verKey);
                emailServiceComponent.sendVerificationEmail(customerPasswordResetObject.getUsername(), verKey);
                customerRepository.save(theCustomer);
                status = HttpStatus.OK;
            }else{
                status = HttpStatus.NOT_FOUND;
            }
        }catch(Exception e){
            e.printStackTrace();
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity(status);
    }

    /*
    //Get the Customer from the db (matched by email)
    //Make A token if the email and password combination matches
    //Return the token and OK status if all goes well
    */
    @PutMapping("/verify-login")
    public ResponseEntity<AuthResponse> verifyUserLogin(@RequestBody CustomerVerificationObject inputCustomer){
        HttpStatus status;
        AuthResponse resToken = null;
        try {
            Customer customer = customerRepository.findCustomerByEmail(inputCustomer.getEmail());
            if (customer.getVerificationKey().equals(inputCustomer.getVerificationKey())) {
                Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                inputCustomer.getEmail(),
                                inputCustomer.getPassword()
                        )
                );

                SecurityContextHolder.getContext().setAuthentication(authentication);

                String token = tokenProvider.createToken(authentication);
                resToken = new AuthResponse(token);
                status = HttpStatus.OK;
            } else {
                status = HttpStatus.BAD_REQUEST;
            }
        }
        catch(Exception e){
            e.printStackTrace();
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(resToken, status);
    }

    /*
    //Get the customer based on the email
    //Checks that the verificationkeys matches
    //Saves the customers as verified in the database
    //Returnes OK if all goes well
     */
    @PutMapping("/verify-register")
    public ResponseEntity verifyUserRegistration(@RequestBody CustomerVerificationObject inputCustomer){
        HttpStatus status;
        try {
            Customer customer = customerRepository.findCustomerByEmail(inputCustomer.getEmail());
            if (customer.getVerificationKey().equals(inputCustomer.getVerificationKey())) {
                customer.setVerified(true);
                customerRepository.save(customer);
                status = HttpStatus.OK;
            } else {
                status = HttpStatus.BAD_REQUEST;
            }
        }
        catch(Exception e){
            e.printStackTrace();
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity(status);
    }


}
