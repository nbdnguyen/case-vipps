package no.noroff.vipps.controllers;


import no.noroff.vipps.models.Customer;
import no.noroff.vipps.models.Item;
import no.noroff.vipps.models.Receipt;
import no.noroff.vipps.payload.ReceiptPayload;
import no.noroff.vipps.repositories.CustomerRepository;
import no.noroff.vipps.repositories.ItemRepository;
import no.noroff.vipps.repositories.ReceiptRepository;
import no.noroff.vipps.security.CurrentUser;
import no.noroff.vipps.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/v1/order")
public class ReceiptController {

    @Autowired
    private ReceiptRepository receiptRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ItemRepository itemRepository;

    @GetMapping
    public ResponseEntity<List<Receipt>> getAllReceipts(@CurrentUser UserPrincipal userPrincipal) {
        List<Receipt> receipts = receiptRepository.findAllByCustomer_CustomerId(userPrincipal.getId());
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(receipts, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getReceipt(@PathVariable Long id, @CurrentUser UserPrincipal userPrincipal) {
        Receipt returnReceipt = new Receipt();
        HttpStatus status;
        if(receiptRepository.existsById(id)) {
            status = HttpStatus.OK;
            if(receiptRepository.findByCustomer_CustomerIdAndReceiptId(userPrincipal.getId(), id) == null){
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("you do not own a receipt with that id");
            }
            returnReceipt = receiptRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnReceipt, status);
    }


    /**
     * POST is accessible for not authenticated users.
     * @param receiptPayload contains status and a list of itemIds
     * @param userPrincipal the logged in user, might be null
     * @return 201 with a receipt object.
     * @return 400 if there is not items
     */
    @PostMapping()
    public ResponseEntity<?> addNewReceipt(@RequestBody ReceiptPayload receiptPayload, @CurrentUser UserPrincipal userPrincipal) {
        Receipt receipt = new Receipt();
        Customer customer = new Customer();
        if(userPrincipal != null) {
            customer = customerRepository.findByCustomerId(userPrincipal.getId());
            receipt.setCustomer(customer);
        }else{
            String[] names = receiptPayload.getName().split(" ");
            customer.setFamilyName(names[names.length-1]);
            customer.setEmail(receiptPayload.getEmail());
            customer.setAddress(receiptPayload.getAddress());
            Customer recieptCustomer = customerRepository.save(customer);
            receipt.setCustomer(recieptCustomer);
        }
        if(receiptRepository.findByIdemKey(receiptPayload.getIdemKey()) != null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("A receipt with this ID already exists");
        }

        receipt.setTime(new Timestamp(System.currentTimeMillis()));
        double thePrice = 0;
        if (receiptPayload.getItemsIDs() != null) {
            if(receiptPayload.getItemsIDs().size() == 0){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("must at least contain one item");
            }
            List<Item> items = itemRepository.findAllById(receiptPayload.getItemsIDs());
            for(int i = 0; i < items.size(); i++){
                thePrice += items.get(i).getPrice();
            }
            receipt.setItems(items);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("missing itemIds");
        }
        String statusString = receiptPayload.getStatus().toUpperCase();
        if( statusString.equals("COMPLETE") || statusString.equals("IN_PROGRESS") || statusString.equals("FAILED")) {
            receipt.setStatus(statusString);
        }else{
            receipt.setStatus("INVALID_STATUS");
        }
        receipt.setTotalPrice(thePrice);
        receipt.setIdemKey(receiptPayload.getIdemKey());
        Receipt returnReceipt = receiptRepository.save(receipt);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnReceipt, status);
    }

    @PutMapping()
    public ResponseEntity<?> updateReceipt(@RequestBody ReceiptPayload receiptPayload, @CurrentUser UserPrincipal userPrincipal) {
        Receipt returnReceipt;
        HttpStatus status;
        returnReceipt = receiptRepository.findByIdemKey(receiptPayload.getIdemKey());
        if(returnReceipt == null) {
            status = HttpStatus.NOT_FOUND;
        }else {
            returnReceipt.setStatus(receiptPayload.getStatus());
            receiptRepository.save(returnReceipt);
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(returnReceipt, status);
    }



    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteReceipt(@PathVariable Long id, @CurrentUser UserPrincipal userPrincipal) {
        HttpStatus status;
        if(receiptRepository.existsById(id)) {
            status = HttpStatus.NO_CONTENT;
            Receipt receipt = receiptRepository.findByCustomer_CustomerIdAndReceiptId(userPrincipal.getId(), id);
            if (receipt == null) {
                status = HttpStatus.UNAUTHORIZED;
            }else {
                receiptRepository.deleteById(id);
            }
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }

}