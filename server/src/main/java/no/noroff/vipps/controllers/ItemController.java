package no.noroff.vipps.controllers;

import org.hibernate.Hibernate;
import org.hibernate.Transaction;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.provider.HibernateUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import no.noroff.vipps.models.Item;
import no.noroff.vipps.repositories.ItemRepository;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/item")
public class ItemController {

    @Autowired
    private ItemRepository itemRepository;

    /**
     * Does not require authentication
     * @return list of all the items in the database
     */
    @GetMapping
    public ResponseEntity<List<Item>> getAllItems() {
        List<Item> items = itemRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(items, status);
    }

    /**
     * Does not require authentication
     * @param id of the item
     * @return item of the specified id.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Item> getItem(@PathVariable Long id) {
        Item returnItem = new Item();
        HttpStatus status;
        if(itemRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnItem = itemRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnItem, status);
    }


    /**
     * Does not require authentication
     * @return 0 - 4 random items
     */
    @GetMapping("/random")
    public ResponseEntity<List<Item>> getRandomItems(){
        //Generate random number between 0-4
        //Add number to sql query
        //Send request for that number of items
        //Return itemlist
        Random rand = new Random();
        int randInt = rand.nextInt(5);
        List<Item> items = itemRepository.findRandomItems(randInt);
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(items, status);
    }

}