package no.noroff.vipps.controllers;

import no.noroff.vipps.exception.ResourceNotFoundException;
import no.noroff.vipps.models.Customer;
import no.noroff.vipps.payload.CustomerPasswordResetObject;
import no.noroff.vipps.payload.CustomerVerificationObject;
import no.noroff.vipps.payload.UpdateCustomerPayload;
import no.noroff.vipps.security.CurrentUser;
import no.noroff.vipps.security.UserPrincipal;
import no.noroff.vipps.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/customer")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    /**
     *
     * @param userPrincipal the customer that is logged in
     * @return the customer with the same customer id as the userPrincipal Id.
     */
    @GetMapping
    public ResponseEntity<Customer> getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        HttpStatus status = HttpStatus.OK;
        Customer returnCustomer = new Customer();
        if (customerRepository.findById(userPrincipal.getId()).isPresent()) {
            returnCustomer = customerRepository.findById(userPrincipal.getId()).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCustomer, status);
    }


    /**
     *
     * @param userPrincipal the customer that is logged in (provided by the authentication manager)
     * @param updateCustomerPayload json payload from client.
     * @return Httpsstatus ok and customer if everything went okay
     * @return HttpsStatus bad request if somebody is attempting to update a customer they are not logged in as.
     * @return HttpStatus bad request if updateCustomerPayload is not validated
     */
    @PutMapping
    public ResponseEntity<Customer> updateCustomer(
            @CurrentUser UserPrincipal userPrincipal, @RequestBody UpdateCustomerPayload updateCustomerPayload) {
        Customer customer = new Customer();
        Customer returnCustomer = null;
        Long id = userPrincipal.getId();
        HttpStatus status;

        if(!customerRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
        } else {
            if (updateCustomerPayload == null || updateCustomerPayload.getCustomerId() != id){
                status = HttpStatus.BAD_REQUEST;
            } else {
                //Updates the existing customer with the new info.
                //Since all the variables will be sent from the form,
                //Tests to see if they are empty or too short.
                status = HttpStatus.OK;
                customer = customerRepository.findByCustomerId(userPrincipal.getId());
                if(updateCustomerPayload.getGivenName().length() >= 2) {
                    customer.setGivenName(updateCustomerPayload.getGivenName());
                } else {
                    status = HttpStatus.BAD_REQUEST;
                }
                if(updateCustomerPayload.getFamilyName().length() >= 2) {
                    customer.setFamilyName(updateCustomerPayload.getFamilyName());
                } else {
                    status = HttpStatus.BAD_REQUEST;
                }
                if(updateCustomerPayload.getAddress().length() >= 6) {
                    customer.setAddress(updateCustomerPayload.getAddress());
                } else {
                    status = HttpStatus.BAD_REQUEST;
                }
                if(updateCustomerPayload.getPhone().length() >= 8) {
                    customer.setPhone(updateCustomerPayload.getPhone());
                } else {
                    status = HttpStatus.BAD_REQUEST;
                }
                returnCustomer = customerRepository.save(customer);
            }
        }
        return new ResponseEntity<>(returnCustomer, status);
    }

    /**
     *
     * @param userPrincipal the user that is logged in
     * @return httpstatus ok if the customer is successfully anonymised.
     * @return httpstatus not found if the customer does not exist.
     */
    @DeleteMapping
    public ResponseEntity<Customer> deleteCustomer(@CurrentUser UserPrincipal userPrincipal) {
        /**
        * This delete function will not delete the current customer, only anonymise all fields with information
        */
        Long id = userPrincipal.getId();
        Customer returnCustomer = new Customer();
        Customer user = customerRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
        Customer anonymous = new Customer(
                id, "anonymous", "anonymous", "anonymous", "anonymous", "anonymous", false);
        HttpStatus status;
        if(!customerRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
        } else {
            if (user.getProviderId() != null) {
                anonymous.setProviderId("anonymous");
            }
            returnCustomer = customerRepository.save(anonymous);
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(anonymous, status);
    }

    /**
     *
     * @param inputCustomer the customer that will be validated
     * @return http status ok  and updated customer if everything went okay
     * @return http status bad request if the customer couldn't be validated
     */
    @PutMapping("/verify")
    public ResponseEntity<Customer> updateVerified(@RequestBody CustomerVerificationObject inputCustomer){
        Customer customer = customerRepository.findCustomerByEmail(inputCustomer.getEmail());
        Customer returnCustomer = new Customer();
        HttpStatus status;
        if(customer.getVerificationKey().equals(inputCustomer.getVerificationKey())){
            customer.setVerified(true);
            returnCustomer = customerRepository.save(customer);
            status = HttpStatus.OK;
        } else{
            status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(returnCustomer, status);
    }

    @PutMapping("/resetpassword")
    public ResponseEntity resetPassword(@RequestBody CustomerPasswordResetObject inputCustomer){
        HttpStatus status;
        Customer theCustomer = null;
        try{
            if(customerRepository.existsByEmail(inputCustomer.getUsername())){
                theCustomer = customerRepository.findCustomerByEmail(inputCustomer.getUsername());
                if(theCustomer.getVerificationKey().equals(inputCustomer.getVerificationKey())){
                    theCustomer.setPassword(passwordEncoder.encode(inputCustomer.getPassword()));
                    customerRepository.save(theCustomer);
                    status = HttpStatus.OK;
                }else{
                    status = HttpStatus.BAD_REQUEST;
                }
            }else{
                status = HttpStatus.NOT_FOUND;
            }
        }catch(Exception e){
            e.printStackTrace();
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity(status);
    }
}