package no.noroff.vipps.controllers;


import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrivateController {

    @GetMapping("/private")
    public String privatePlace(@AuthenticationPrincipal OAuth2User principal) {
        return "welcome to the private endpoint";
    }

    @GetMapping("/public")
    public String publicEndpoint(){
        return "welcome to public endpoint";
    }

}
