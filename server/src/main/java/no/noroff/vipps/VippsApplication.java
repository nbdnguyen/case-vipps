 package no.noroff.vipps;

import no.noroff.vipps.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class VippsApplication {

	public static void main(String[] args) {
		System.out.println("Starting app...");
		SpringApplication.run(VippsApplication.class, args);
		System.out.println("Finished startup.");
	}

}
